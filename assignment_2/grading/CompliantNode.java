import java.util.ArrayList;
import java.util.Set;
import java.util.Arrays;

public class CompliantNode implements Node {

	private Set<Transaction> pendingTransactions;
	private boolean[] followees;
	
    // NOTE: Node is an interface and does not have a constructor.
    // However, your CompliantNode.java class requires a 4 argument
    // constructor as defined in Simulation.java
	public CompliantNode(
		double p_graph,
		double p_malicious,
		double p_txDistribution,
		int numRounds)
	{
	}

    // followees[i] is True iff this node follows node i
    public void setFollowees(boolean[] followees)
    {
		this.followees = followees;
		//System.out.println("fwe: " + followees.length);
		//System.out.println("fwee: " + Arrays.toString(followees));
    }

    //initialize proposal list of transactions
    public void setPendingTransaction(Set<Transaction> pendingTransactions)
    {
		this.pendingTransactions = pendingTransactions;
    }

    // return proposals to send to my followers.
    // REMEMBER: After final round, behavior of getProposals changes and
    // it should return the transactions upon which consensus has been reached.
    public Set<Transaction> getProposals()
    {
 		return pendingTransactions;
    }

    // receive candidates from other nodes.
	public void receiveCandidates(ArrayList<Integer[]> candidates)
	{
    	//System.out.println("Candidates: " + candidates.size());
		for (Integer[] candidate : candidates)
		{
			//System.out.println("receiveCandidates: " + Arrays.toString(c));
			//if (followees[candidate[1]])
			//{
				Transaction tx = new Transaction(candidate[0]);
            	if ( ! this.pendingTransactions.contains(tx))
            	{
               		this.pendingTransactions.add(tx);
				}
				//pendingTransactions.add(new Transaction(c[0]));
			//}
		}
	}
}
