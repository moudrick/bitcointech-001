import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Comparator;

/*
Extra Credit:​Create a second file called MaxFeeTxHandler.java with similar API whose handleTxs finds a set of transactions with maximum total transaction fees (i.e. maximize the sum over all transactions in the set of (sum of input values - sum of output values)). Finding such a set of transactions is NP hard and thus find only a heuristic which might increase the total transaction fees.
*/
public class MaxFeeTxHandler {
	public UTXOPool ledger;

	/* Creates a public ledger whose current UTXOPool (collection of unspent 
	 * transaction outputs) is utxoPool. This should make a defensive copy of 
	 * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
	 */
	public MaxFeeTxHandler(UTXOPool utxoPool) {
		ledger = new UTXOPool(utxoPool);
	}

	/* Returns true if 
	 * (1) all outputs claimed by tx are in the current UTXO pool, 
	 * (2) the signatures on each input of tx are valid, 
	 * (3) no UTXO is claimed multiple times by tx, 
	 * (4) all of tx’s output values are non-negative, and
	 * (5) the sum of tx’s input values is greater than or equal to the sum of   
	        its output values;
	   and false otherwise.
	 */

	public boolean isValidTx(Transaction tx) {
		ArrayList<UTXO> utxoUnique = new ArrayList<UTXO>();
		double totalInputs = 0.0;
		for (int i = 0; i < tx.numInputs(); ++i)
		{
			Transaction.Input ti = tx.getInput(i);
			UTXO utxo = new UTXO(ti.prevTxHash, ti.outputIndex);
			if ( ! ledger.contains(utxo) ) return false;
			if (utxoUnique.contains(utxo)) return false;
			utxoUnique.add(utxo);
			
			Transaction.Output to = ledger.getTxOutput(utxo);
			boolean verified = to.address.verifySignature(tx.getRawDataToSign(i), ti.signature);
			if ( ! verified ) return false;
			
			totalInputs += to.value;
		}
		
		double totalOutputs = 0.0;
		for (int j = 0; j < tx.numOutputs(); ++j)
		{
			Transaction.Output to = tx.getOutput(j);
			if (to.value < 0.0) return false;
			totalOutputs += to.value;
		}
		
		if (totalOutputs > totalInputs) return false;
		
		return true;
	}

	class TransactionWithFees
	{
		private double fees;
  		private Transaction tx;
		
		public TransactionWithFees(double fees, Transaction tx) {
    		this.fees = fees;
  	  		this.tx = tx;
		}

  		public double getFees() {
    		return fees;
  		}
		
		@Override
  		public String toString() {
    		return "(" + fees + ", " + 1 + ")";
  		}
	}
	/* Handles each epoch by receiving an unordered array of proposed
	 * transactions, checking each transaction for correctness, 
	 * returning a mutually valid array of accepted transactions, 
	 * and updating the current UTXO pool as appropriate.
	 */
	public Transaction[] handleTxs(Transaction[] possibleTxs) {
		SortedSet<TransactionWithFees> txByFees = new TreeSet<>(Comparator.comparing(TransactionWithFees::getFees));

		for (Transaction tx : possibleTxs)
		{
			double totalInputValue = 0.0;
			for (Transaction.Input ti : tx.getInputs())
			{
				UTXO utxo = new UTXO(ti.prevTxHash, ti.outputIndex);
				Transaction.Output to = ledger.getTxOutput(utxo);
				if (to == null  || ! isValidTx(tx))
				{
					continue;
				}
				totalInputValue += to.value;
			}
			int i = 0;
			double totalOutputValue = 0.0;
			for (Transaction.Output to : tx.getOutputs())
			{
				totalOutputValue += to.value;
			}
			double TTfees = totalInputValue - totalOutputValue;
			txByFees.add(new TransactionWithFees( -TTfees, tx));
		}
		//txByFees.forEach(System.out::println);
		
	
		ArrayList<Transaction> acceptedTxs = new ArrayList<Transaction>();
		for (TransactionWithFees txf : txByFees)
		{
			Transaction tx = txf.tx;
			if (isValidTx(tx))
			{
				boolean accept = true;
				ArrayList<UTXO> spentUTXO = new ArrayList<UTXO>();
				for (Transaction.Input ti : tx.getInputs())
				{
					UTXO utxo = new UTXO(ti.prevTxHash, ti.outputIndex);
					if (ledger.contains(utxo))
					{
						spentUTXO.add(utxo);
					}
					else
					{
						accept = false;
					}
				}
				
				if (accept)
				{
					acceptedTxs.add(tx);
					
					for (UTXO utxo : spentUTXO)
					{
						ledger.removeUTXO(utxo);
					}
				
					int i = 0;
					for (Transaction.Output to : tx.getOutputs())
					{
						ledger.addUTXO(new UTXO(tx.getHash(), i++), to);
					}
				}
			}
		}
		return acceptedTxs.toArray(new Transaction[acceptedTxs.size()]);
	}
}

