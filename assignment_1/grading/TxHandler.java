import java.util.ArrayList;

public class TxHandler {
	public UTXOPool ledger;

	/* Creates a public ledger whose current UTXOPool (collection of unspent 
	 * transaction outputs) is utxoPool. This should make a defensive copy of 
	 * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
	 */
	public TxHandler(UTXOPool utxoPool) {
		ledger = new UTXOPool(utxoPool);
	}

	/* Returns true if 
	 * (1) all outputs claimed by tx are in the current UTXO pool, 
	 * (2) the signatures on each input of tx are valid, 
	 * (3) no UTXO is claimed multiple times by tx, 
	 * (4) all of tx’s output values are non-negative, and
	 * (5) the sum of tx’s input values is greater than or equal to the sum of   
	        its output values;
	   and false otherwise.
	 */

	public boolean isValidTx(Transaction tx) {
		ArrayList<UTXO> utxoUnique = new ArrayList<UTXO>();
		double totalInputs = 0.0;
		for (int i = 0; i < tx.numInputs(); ++i)
		{
			Transaction.Input ti = tx.getInput(i);
			UTXO utxo = new UTXO(ti.prevTxHash, ti.outputIndex);
			if ( ! ledger.contains(utxo) ) return false;
			if (utxoUnique.contains(utxo)) return false;
			utxoUnique.add(utxo);
			
			Transaction.Output to = ledger.getTxOutput(utxo);
			boolean verified = to.address.verifySignature(tx.getRawDataToSign(i), ti.signature);
			if ( ! verified ) return false;
			
			totalInputs += to.value;
		}
		
		double totalOutputs = 0.0;
		for (int j = 0; j < tx.numOutputs(); ++j)
		{
			Transaction.Output to = tx.getOutput(j);
			if (to.value < 0.0) return false;
			totalOutputs += to.value;
		}
		
		if (totalOutputs > totalInputs) return false;
		
		return true;
	}

	/* Handles each epoch by receiving an unordered array of proposed
	 * transactions, checking each transaction for correctness, 
	 * returning a mutually valid array of accepted transactions, 
	 * and updating the current UTXO pool as appropriate.
	 */
	public Transaction[] handleTxs(Transaction[] possibleTxs) {
		ArrayList<Transaction> acceptedTxs = new ArrayList<Transaction>();
		for (Transaction tx : possibleTxs)
		{
			if (isValidTx(tx))
			{
				boolean accept = true;
				ArrayList<UTXO> spentUTXO = new ArrayList<UTXO>();
				for (Transaction.Input ti : tx.getInputs())
				{
					UTXO utxo = new UTXO(ti.prevTxHash, ti.outputIndex);
					if (ledger.contains(utxo))
					{
						spentUTXO.add(utxo);
					}
					else
					{
						accept = false;
					}
				}
				
				if (accept)
				{
					acceptedTxs.add(tx);
					
					for (UTXO utxo : spentUTXO)
					{
						ledger.removeUTXO(utxo);
					}
				
					int i = 0;
					for (Transaction.Output to : tx.getOutputs())
					{
						ledger.addUTXO(new UTXO(tx.getHash(), i++), to);
					}
				}
			}
		}
		return acceptedTxs.toArray(new Transaction[acceptedTxs.size()]);
	}
}

